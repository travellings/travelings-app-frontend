import * as API from '../constants';

export const fetchTravelings = async ({ fetchData }) => {
  const bodyRequest = JSON.stringify(fetchData);

  const headersRequest = new Headers({
    'Content-Type': 'application/json',
  });

  const optionsRequest = {
    method: 'POST',
    headers: headersRequest,
    body: bodyRequest,
    redirect: 'follow',
  };

  const res = await fetch(API.BASE_URL, optionsRequest);

  return res.json();
};

export const createTraveling = async (travelingData) => {
  const bodyRequest = JSON.stringify(travelingData);

  const headersRequest = new Headers({
    'Content-Type': 'application/json',
  });

  const optionsRequest = {
    method: 'POST',
    headers: headersRequest,
    body: bodyRequest,
    redirect: 'follow',
  };

  const res = await fetch(`${API.BASE_URL}/create`, optionsRequest);
  return res.json();
};

export const editTraveling = async (travelingData) => {
  const bodyRequest = JSON.stringify(travelingData);

  const headersRequest = new Headers({
    'Content-Type': 'application/json',
  });

  const optionsRequest = {
    method: 'PUT',
    headers: headersRequest,
    body: bodyRequest,
    redirect: 'follow',
  };

  const url = `${API.BASE_URL}/${travelingData.id}`;
  const res = await fetch(url, optionsRequest);

  return await res.json();
};

export const deleteTraveling = async (travelingId) => {
  const optionsRequest = {
    method: 'DELETE',
    redirect: 'follow',
  };

  const url = `${API.BASE_URL}/${travelingId}`;
  const res = await fetch(url, optionsRequest);
  const deletedTravelingId = await res.json();

  return deletedTravelingId;
};
