import * as TYPES from './types';

export const handleError = (error) => ({
  type: TYPES.IS_ERROR,
  payload: { error },
});
