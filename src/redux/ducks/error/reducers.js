import * as TYPES from './types';

const initialState = {
  isError: false,
  errorMessage: '',
};

export const error = (state = initialState, { type, payload }) => {
  switch (type) {
    case TYPES.IS_ERROR: {
      return payload.error;
    }
    default: {
      return state;
    }
  }
};
