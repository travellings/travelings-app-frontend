import * as TYPES from './types';

export const handleModal = (isOpenModal) => ({
  type: TYPES.IS_OPEN_MODAL,
  payload: { isOpenModal },
});
