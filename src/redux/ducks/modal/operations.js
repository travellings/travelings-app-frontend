import { setCurrentSelectedTraveling } from '../travelings';
import { handleModal } from './actions';

export const closeModal = (dispatch) => {
  dispatch(setCurrentSelectedTraveling({}));
  dispatch(handleModal(false));
};
