import * as TYPES from './types';

export const isOpenModal = (state = false, { type, payload }) => {
  switch (type) {
    case TYPES.IS_OPEN_MODAL: {
      return payload.isOpenModal;
    }
    default: {
      return state;
    }
  }
};
