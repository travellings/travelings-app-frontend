import * as actions from './actions';
import * as api from '../../../api/api';
import { hanldeLoading } from '../loading';
import { handleError } from '../error';

export const fetchTravelings = (travelingOptions) => async (dispatch) => {
  try {
    dispatch(
      hanldeLoading({
        isLoading: true,
        coverFullScreen: travelingOptions.coverFullScreen,
      })
    );

    const res = await api.fetchTravelings(travelingOptions);
    dispatch(actions.getTravelings(res));

    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: travelingOptions.coverFullScreen,
      })
    );
  } catch (error) {
    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: travelingOptions.coverFullScreen,
      })
    );

    dispatch(
      handleError({
        isError: true,
        errorMessage: 'Something went wrong. Try again later',
      })
    );
  }
};

export const createTraveling = (travelingData) => async (dispatch) => {
  try {
    dispatch(
      hanldeLoading({
        isLoading: true,
        coverFullScreen: true,
      })
    );

    const res = await api.createTraveling(travelingData);
    dispatch(actions.addCreatedTraveling(res));

    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );
  } catch (error) {
    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );

    dispatch(
      handleError({
        isError: true,
        errorMessage: 'Something went wrong. Try again later',
      })
    );
  }
};

export const editTraveling = (travelingData) => async (dispatch) => {
  try {
    dispatch(
      hanldeLoading({
        isLoading: true,
        coverFullScreen: true,
      })
    );

    const res = await api.editTraveling(travelingData);
    dispatch(actions.updateSingleTraveling(res));

    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );
  } catch (error) {
    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );

    dispatch(
      handleError({
        isError: true,
        errorMessage: 'Something went wrong. Try again later',
      })
    );
  }
};

export const deleteTraveling = (travelingId) => async (dispatch) => {
  try {
    dispatch(
      hanldeLoading({
        isLoading: true,
        coverFullScreen: true,
      })
    );

    const res = await api.deleteTraveling(travelingId);
    dispatch(actions.deleteSingleTraveling(res));

    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );
  } catch (error) {
    dispatch(
      hanldeLoading({
        isLoading: false,
        coverFullScreen: true,
      })
    );

    dispatch(
      handleError({
        isError: true,
        errorMessage: 'Something went wrong. Try again later',
      })
    );
  }
};
