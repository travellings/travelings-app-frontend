import * as TYPES from './types';

export const getTravelings = (travelings) => ({
  type: TYPES.FETCH_TRAVELINGS,
  payload: { travelings },
});

export const addCreatedTraveling = (traveling) => ({
  type: TYPES.ADD_CREATED_TRAVELING,
  payload: { traveling },
});

export const setCurrentSelectedTraveling = (currentSelectedTraveling) => ({
  type: TYPES.SET_CURRENT_SELECTED_TRAVELING,
  payload: { currentSelectedTraveling },
});

export const updateSingleTraveling = (updatedTraveling) => ({
  type: TYPES.UPDATE_SINGLE_TRAVELING,
  payload: { updatedTraveling },
});

export const deleteSingleTraveling = (travelingId) => ({
  type: TYPES.DELETE_SINGLE_TRAVELING,
  payload: { travelingId },
});
