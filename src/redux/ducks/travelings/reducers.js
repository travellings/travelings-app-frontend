import * as TYPES from './types';
import { convertDateFormat } from '../../../utils';
import { YYYY_MM_DD } from '../../../constants';

const initialState = {
  travelingsList: [],
  currentSelectedTraveling: {},
  travelingsTotalCount: 0,
};

export const travelings = (state = initialState, { type, payload }) => {
  switch (type) {
    case TYPES.FETCH_TRAVELINGS: {
      return {
        ...state,
        travelingsTotalCount: payload.travelings.totalItems,
        travelingsList: payload.travelings.travelings,
      };
    }
    case TYPES.ADD_CREATED_TRAVELING: {
      const formattedDatesTraveling = {
        ...payload.traveling,
        arrival: convertDateFormat({
          date: payload.traveling.arrival,
          format: YYYY_MM_DD,
        }),
        departure: convertDateFormat({
          date: payload.traveling.departure,
          format: YYYY_MM_DD,
        }),
      };

      return {
        ...state,
        travelingsList: state.travelingsList.concat(formattedDatesTraveling),
      };
    }
    case TYPES.SET_CURRENT_SELECTED_TRAVELING: {
      return {
        ...state,
        currentSelectedTraveling: payload.currentSelectedTraveling,
      };
    }
    case TYPES.UPDATE_SINGLE_TRAVELING: {
      return {
        ...state,
        travelingsList: state.travelingsList.map((traveling) => {
          if (traveling.id === payload.updatedTraveling.id) {
            return payload.updatedTraveling;
          }
          return traveling;
        }),
      };
    }
    case TYPES.DELETE_SINGLE_TRAVELING: {
      return {
        ...state,
        travelingsList: state.travelingsList.filter(
          (traveling) => traveling.id !== payload.travelingId.id
        ),
      };
    }
    default: {
      return state;
    }
  }
};
