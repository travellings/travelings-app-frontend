import { combineReducers } from 'redux';
import { travelings } from './travelings';
import { loading } from './loading';
import { isOpenModal } from './modal';
import { error } from './error';

const rootReducer = combineReducers({
  travelings,
  loading,
  isOpenModal,
  error,
});

export default rootReducer;
