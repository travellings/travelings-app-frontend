import * as TYPES from './types';

export const hanldeLoading = ({ isLoading, coverFullScreen }) => ({
  type: TYPES.IS_LOADING,
  payload: { isLoading, coverFullScreen },
});
