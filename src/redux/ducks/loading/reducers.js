import * as TYPES from './types';

export const loading = (state = false, { type, payload }) => {
  switch (type) {
    case TYPES.IS_LOADING: {
      return payload;
    }
    default: {
      return state;
    }
  }
};
