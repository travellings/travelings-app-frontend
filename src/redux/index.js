export { handleError, error } from './ducks/error';
export { hanldeLoading, loading } from './ducks/loading';
export { handleModal, isOpenModal, closeModal } from './ducks/modal';
export {
  fetchTravelings,
  createTraveling,
  travelings,
  setCurrentSelectedTraveling,
  editTraveling,
  deleteTraveling,
} from './ducks/travelings';
export { configureStore } from './store/configureStore';
