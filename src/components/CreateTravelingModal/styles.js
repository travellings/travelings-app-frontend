import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  root: {
    width: 400,
    padding: '16px 32px',
    backgroundColor: '#fff',
    outline: 'none',
    position: 'absolute',
    top: '50%',
    left: '50%',
    zIndex: 1,
    transform: 'translate(-50%, -50%)',
  },
  closeIcon: {
    color: '#3f51b5',
    cursor: 'pointer',
    position: 'absolute',
    top: 15,
    right: 15,
    zIndex: 1,
    transition: 'color .3s linear',
    '&:hover': {
      color: '#303f9f',
    },
  },
  addButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    zIndex: 1,
  },
});
