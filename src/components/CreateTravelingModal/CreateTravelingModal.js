import React from 'react';
import { connect } from 'react-redux';
import {
  createTraveling,
  editTraveling,
  handleModal,
  closeModal,
} from '../../redux';
import Card from '@material-ui/core/Card';
import Modal from '@material-ui/core/Modal';
import CardContent from '@material-ui/core/CardContent';
import Fab from '@material-ui/core/Fab';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Form from '../Form';
import { useStyles } from './styles';

const CreateTravelingModal = ({
  actions,
  isOpenModal,
  currentSelectedTraveling,
}) => {
  const classes = useStyles();

  const hideModal = () => actions.closeModal();
  const handleModalOpen = (bool) => () => actions.handleModalVisibility(bool);

  return (
    <div>
      <Modal open={isOpenModal} onClose={hideModal}>
        <Card variant='outlined' className={classes.root}>
          <FontAwesomeIcon
            icon={faTimes}
            size='lg'
            className={classes.closeIcon}
            onClick={hideModal}
          />
          <CardContent>
            <Form
              handleModalVisibility={actions.handleModalVisibility}
              createTraveling={actions.createTraveling}
              openModal={isOpenModal}
              currentSelectedTraveling={currentSelectedTraveling}
              editTraveling={actions.editTraveling}
              hideModal={hideModal}
            />
          </CardContent>
        </Card>
      </Modal>
      <Fab
        color='primary'
        className={classes.addButton}
        onClick={handleModalOpen(true)}
      >
        <FontAwesomeIcon icon={faPlus} size='lg' />
      </Fab>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isOpenModal: state.isOpenModal,
});

const mapDispatchToProps = (dispatch) => ({
  actions: {
    handleModalVisibility: (modalVisibility) =>
      dispatch(handleModal(modalVisibility)),
    createTraveling: (travelingData) =>
      dispatch(createTraveling(travelingData)),
    editTraveling: (updatedTraveling) =>
      dispatch(editTraveling(updatedTraveling)),
    closeModal: () => dispatch(closeModal),
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTravelingModal);
