import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
  root: {
    display: "block",
    alignSelf: "center",
    margin: 10,
  },
});
