import React, { forwardRef } from 'react';
import Button from '@material-ui/core/Button';
import { useStyles } from './styles';

const PrimaryButton = forwardRef(({ children, type, handleClick }, ref) => {
  const classes = useStyles();

  return (
    <Button
      ref={ref}
      type={type}
      variant='contained'
      color='primary'
      className={classes.root}
      onClick={handleClick}
    >
      {children}
    </Button>
  );
});

export default PrimaryButton;
