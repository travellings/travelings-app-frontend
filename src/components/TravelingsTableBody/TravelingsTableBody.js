import React from "react";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import ConfigureTravelingMenu from "../ConfigureTravelingMenu";
import { D_MMMM_YYYY } from "../../constants";
import { convertDateFormat } from "../../utils";
import { useStyles } from "./styles";

const TravelingsTableBody = ({
  currentPageNumber,
  rowsPerPage,
  openConfigureTravelingMenu,
  anchorElementMenu,
  closeConfigureTravelingMenu,
  openEditTravelingModal,
  deleteTraveling,
  hideModal,
  finalTravelingsList,
}) => {
  const classes = useStyles();

  return (
    <TableBody>
      {finalTravelingsList.map(
        ({ id, country, city, arrival, departure }, index) => (
          <TableRow key={id}>
            <TableCell component="th" scope="row" align="right">
              {index + 1 + rowsPerPage * currentPageNumber}
            </TableCell>
            <TableCell align="right">{country}</TableCell>
            <TableCell align="right">{city}</TableCell>
            <TableCell align="right">
              {convertDateFormat({ date: arrival, format: D_MMMM_YYYY })}
            </TableCell>
            <TableCell align="right">
              {convertDateFormat({ date: departure, format: D_MMMM_YYYY })}
            </TableCell>
            <TableCell align="right">
              <IconButton
                size="small"
                onClick={openConfigureTravelingMenu({
                  id,
                  country,
                  city,
                  arrival,
                  departure,
                })}
              >
                <FontAwesomeIcon
                  icon={faEllipsisV}
                  className={classes.moreIcon}
                />
              </IconButton>
            </TableCell>
          </TableRow>
        )
      )}
      <TableRow className={classes.configureMenuRow}>
        <TableCell>
          <ConfigureTravelingMenu
            anchorElementMenu={anchorElementMenu}
            closeConfigureTravelingMenu={closeConfigureTravelingMenu}
            openEditTravelingModal={openEditTravelingModal}
            deleteTraveling={deleteTraveling}
            hideModal={hideModal}
          />
        </TableCell>
      </TableRow>
    </TableBody>
  );
};

export default TravelingsTableBody;
