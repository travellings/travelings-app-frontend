import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
  moreIcon: {
    cursor: "pointer",
    color: "#3f51b5",
    "&:hover": {
      color: "#3f51b5",
    },
  },
  configureMenuRow: {
    display: "none",
  },
});
