import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignRight } from "@fortawesome/free-solid-svg-icons";
import { tableHeadCellNameList } from "../../constants";

const TravelingsTableHeader = ({ sortBy, sortOrder, sortHandler }) => {
  return (
    <TableHead>
      <TableRow>
        {tableHeadCellNameList.map(({ id, cellContent, cellKey }) => (
          <TableCell key={id} align="right">
            <TableSortLabel
              active={sortBy === cellKey}
              direction={sortBy === cellKey ? sortOrder : "asc"}
              onClick={sortHandler(cellKey)}
            >
              {cellContent}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="right">
          <FontAwesomeIcon icon={faAlignRight} size="lg" />
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

export default TravelingsTableHeader;
