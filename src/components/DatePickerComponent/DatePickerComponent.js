import React, { useCallback, useState, useEffect, forwardRef } from "react";
import Calendar from "react-calendar";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarDay } from "@fortawesome/free-solid-svg-icons";
import { joinValueInput, convertDateFormat } from "../../utils";
import { DD_MM_YYYY } from "../../constants";
import { useStyles } from "./styles";

const DatePickerComponent = forwardRef(
  ({ isDiscardedChange, ...props }, ref) => {
    const [isDisplayedCalendar, setDisplayedCalendar] = useState(false);
    const [valueCalendar, setValueCalendar] = useState();
    const [valueInput, setValueInput] = useState("");

    const styles = { display: isDisplayedCalendar ? "block" : "none" };
    const classes = useStyles(styles);

    const handleDisplayCalendar = () => {
      setDisplayedCalendar(!isDisplayedCalendar);
    };

    const handleChangeInput = ({ target }) => {
      let joinedValue = joinValueInput(target.value);
      if (joinedValue.length > 10) return;
      setValueInput(joinedValue);
    };

    const handleChangeCalendar = (date) => {
      setValueCalendar(date);
      const formattedDate = convertDateFormat({ date, format: DD_MM_YYYY });
      setValueInput(formattedDate);
      setDisplayedCalendar(false);
    };

    const closeCalendar = useCallback(({ path }) => {
      const conditionClickedOnCalendar = path.some((item) => {
        return item?.className?.indexOf("calendar") > -1;
      });

      if (!conditionClickedOnCalendar) {
        setDisplayedCalendar(false);
        document.removeEventListener("click", closeCalendar);
      }
    }, []);

    useEffect(() => {
      if (isDisplayedCalendar) {
        document.addEventListener("click", closeCalendar);
      }

      return () => {
        document.removeEventListener("click", closeCalendar);
      };
    }, [isDisplayedCalendar, closeCalendar]);

    return (
      <Box className={classes.datePicker}>
        <TextField
          {...props}
          fullWidth
          inputRef={ref}
          value={isDiscardedChange ? "" : valueInput}
          variant="outlined"
          placeholder="DD-MM-YYYY"
          onChange={handleChangeInput}
          InputProps={{
            endAdornment: (
              <InputAdornment
                position="end"
                className={classes.inputAdornment}
                onClick={handleDisplayCalendar}
              >
                <FontAwesomeIcon icon={faCalendarDay} size="lg" />
              </InputAdornment>
            ),
          }}
        />
        <Box className={classes.containerCalendar}>
          <Calendar
            onChange={handleChangeCalendar}
            value={valueCalendar}
            className={classes.calendar}
          />
        </Box>
      </Box>
    );
  }
);

export default DatePickerComponent;
