import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  datePicker: {
    width: '48%',
    position: 'relative',
  },
  inputAdornment: {
    cursor: 'pointer',
  },
  containerCalendar: ({ display }) => ({
    display,
    width: '100%',
    position: 'absolute',
    top: '100%',
    left: 0,
    zIndex: 2,
  }),
  calendar: {
    width: '100%',
    '& .react-calendar__tile--now': {
      background: '#fff',
    },

    '& .react-calendar__tile--now react-calendar__tile--active ': {
      background: '#1087ff',
    },
  },
});
