import React, { useState, forwardRef } from 'react';
import TextField from '@material-ui/core/TextField';
import { useStyles } from '../DatePicker/styles';

const DatePicker = forwardRef(({ value, ...props }, ref) => {
  const [selectedDate, handleDateChange] = useState(value);

  const styles = {
    color: selectedDate ? 'black' : 'transparent',
  };

  const classes = useStyles(styles)();

  const handleChange = ({ target }) => handleDateChange(target.value);

  return (
    <TextField
      classes={classes}
      inputRef={ref}
      type='date'
      variant='outlined'
      value={selectedDate}
      margin='normal'
      onChange={handleChange}
      fullWidth
      {...props}
    />
  );
});

export default DatePicker;
