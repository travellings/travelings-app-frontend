import React from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { useStyles } from './styles';

const ConfirmPopover = ({
  anchorEl,
  closePopover,
  editTraveling,
  travelingData,
  currentSelectedTraveling,
  hideModal,
  deleteTraveling,
  textAction,
}) => {
  const classes = useStyles();
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const makeChangesTraveling = () => {
    editTraveling({ id: currentSelectedTraveling.id, ...travelingData });
    hideModal();
  };

  const removeTraveling = () => {
    deleteTraveling(currentSelectedTraveling.id);
    hideModal();
  };

  const confirmFunction = () => {
    return textAction === 'edit' ? makeChangesTraveling() : removeTraveling();
  };

  return (
    <div>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={closePopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Card>
          <CardContent className={classes.root}>
            Are you sure you want to {textAction} location?
          </CardContent>
          <CardContent className={clsx(classes.root, classes.buttonGroup)}>
            <Button
              size='small'
              variant='contained'
              color='primary'
              className={classes.button}
              onClick={confirmFunction}
            >
              Yes
            </Button>
            <Button
              size='small'
              variant='contained'
              color='primary'
              className={classes.button}
              onClick={closePopover}
            >
              No
            </Button>
          </CardContent>
        </Card>
      </Popover>
    </div>
  );
};

const mapStateToProps = (state) => ({
  currentSelectedTraveling: state.travelings.currentSelectedTraveling,
});

export default connect(mapStateToProps)(ConfirmPopover);
