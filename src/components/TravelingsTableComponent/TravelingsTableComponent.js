import React from "react";
import { connect } from "react-redux";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import PrimarySpinner from "../PrimarySpinner";
import CreateTravelingModal from "../CreateTravelingModal";
import TravelingsTableHeader from "../TravelingsTableHeader";
import TravelingsTableBody from "../TravelingsTableBody";
import TravelingsTablePagination from "../TravelingsTablePagination";
import SearchPanel from "../SearchPanel";
import SecondarySpinner from "../SecondarySpinner";
import { useStyles } from "./styles";

const TravelingsTableComponent = ({
  loading: { isLoading, coverFullScreen },
  travelings,
  isOpenModal,
  travelingsTotalCount,
  tableSettings,
  anchorElementMenu,
  hideModal,
  handleChangePage,
  sortHandler,
  openEditTravelingModal,
  handleChangeRowsPerPage,
  openConfigureTravelingMenu,
  closeConfigureTravelingMenu,
  handleTableSettings,
  deleteTraveling,
}) => {
  const classes = useStyles();

  return (
    <>
      <CreateTravelingModal isOpenModal={isOpenModal} />
      {isLoading && coverFullScreen ? (
        <PrimarySpinner />
      ) : (
        <Box className={classes.content}>
          <SearchPanel
            sortBy={tableSettings.sortBy}
            sortOrder={tableSettings.sortOrder}
            currentPageNumber={tableSettings.currentPageNumber}
            rowsPerPage={tableSettings.rowsPerPage}
            handleTableSettings={handleTableSettings}
            tableSettings={tableSettings}
          />
          {isLoading && !coverFullScreen ? (
            <SecondarySpinner />
          ) : (
            <>
              <TableContainer component={Paper}>
                <Table size="small" aria-label="travelings table">
                  <TravelingsTableHeader
                    sortBy={tableSettings.sortBy}
                    sortOrder={tableSettings.sortOrder}
                    sortHandler={sortHandler}
                  />
                  <TravelingsTableBody
                    finalTravelingsList={travelings}
                    currentPageNumber={tableSettings.currentPageNumber}
                    rowsPerPage={tableSettings.rowsPerPage}
                    openConfigureTravelingMenu={openConfigureTravelingMenu}
                    anchorElementMenu={anchorElementMenu}
                    closeConfigureTravelingMenu={closeConfigureTravelingMenu}
                    openEditTravelingModal={openEditTravelingModal}
                    deleteTraveling={deleteTraveling}
                    hideModal={hideModal}
                  />
                </Table>
              </TableContainer>
              <TravelingsTablePagination
                count={travelingsTotalCount}
                rowsPerPage={tableSettings.rowsPerPage}
                currentPageNumber={tableSettings.currentPageNumber}
                handleChangePage={handleChangePage}
                handleChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </>
          )}
        </Box>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  loading: state.loading,
  travelings: state.travelings.travelingsList,
  travelingsTotalCount: state.travelings.travelingsTotalCount,
  isOpenModal: state.isOpenModal,
});

export default connect(mapStateToProps)(TravelingsTableComponent);
