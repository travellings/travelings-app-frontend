import React, { forwardRef } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const AutocompleteInput = forwardRef(
  ({ options, selectCountry, value, ...props }, ref) => {
    const handleInputChange = (event, value) => selectCountry(value);

    return (
      <Autocomplete
        value={value}
        options={options}
        onInputChange={selectCountry && handleInputChange}
        fullWidth
        freeSolo
        renderInput={(params) => (
          <TextField
            {...params}
            autoFocus={props.name === 'country'}
            variant='outlined'
            inputRef={ref}
            label={props.name.toUpperCase()}
            margin='normal'
            {...props}
          />
        )}
      />
    );
  }
);

export default AutocompleteInput;
