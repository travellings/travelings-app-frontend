import React from 'react';
import TablePagination from '@material-ui/core/TablePagination';

const TravelingsTablePagination = ({
  count,
  rowsPerPage,
  currentPageNumber,
  handleChangePage,
  handleChangeRowsPerPage,
}) => {
  return (
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component='div'
      count={count}
      rowsPerPage={rowsPerPage}
      page={currentPageNumber}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
  );
};

export default TravelingsTablePagination;
