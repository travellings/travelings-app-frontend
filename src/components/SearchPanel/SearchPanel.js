import React, { useState } from "react";
import { connect } from "react-redux";
import { useForm } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faSync } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { fetchTravelings } from "../../redux";
import PrimaryButton from "../PrimaryButton";
import DatePickerComponent from "../DatePickerComponent";
import { reverseDate } from "../../utils";
import { useStyles } from "./styles";
import initialTableState from "../TravelingsTableContainer/initialTableState";

const SearchPanel = ({ actions, handleTableSettings, tableSettings }) => {
  const [isSearchPanelDisplayed, setSearchPanelDisplayed] = useState(false);
  const [isDiscardedChange, setDiscardedChange] = useState(false);

  const classes = useStyles();
  const { register, handleSubmit } = useForm({
    mode: "onSubmit",
    reValidateMode: "onBlur",
  });

  const clearForm = () => setDiscardedChange(true);

  const handleChange = ({ target }) => setSearchPanelDisplayed(target.checked);

  const onSubmit = ({
    startArrival,
    endArrival,
    startDeparture,
    endDeparture,
    country,
    city,
  }) => {
    handleTableSettings({
      country,
      city,
      arrivalRange: {
        startDate: reverseDate(startArrival),
        endDate: reverseDate(endArrival),
      },
      departureRange: {
        startDate: reverseDate(startDeparture),
        endDate: reverseDate(endDeparture),
      },
    });

    actions.fetchTravelings({
      fetchData: {
        ...tableSettings,
        country,
        city,
        arrivalRange: {
          startDate: reverseDate(startArrival),
          endDate: reverseDate(endArrival),
        },
        departureRange: {
          startDate: reverseDate(startDeparture),
          endDate: reverseDate(endDeparture),
        },
      },
      coverFullScreen: false,
    });
  };

  const resetSearchFilters = () => {
    actions.fetchTravelings({
      fetchData: initialTableState,
      coverFullScreen: false,
    });
  };

  return (
    <Box>
      <FormControlLabel
        className={classes.labelSwitcher}
        label="Enable Search Panel"
        labelPlacement="start"
        control={
          <Switch
            checked={isSearchPanelDisplayed}
            onChange={handleChange}
            color="primary"
          />
        }
      />
      {isSearchPanelDisplayed && (
        <Box>
          <Typography className={classes.mainTitle}>Search by</Typography>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Box className={classes.formGroupContainer}>
              <Typography className={classes.subTitle}>
                Country or/and city
              </Typography>
              <Box className={classes.formGroup}>
                <TextField
                  className={classes.textInput}
                  inputRef={register}
                  variant="outlined"
                  name="country"
                  label="Country"
                />
                <TextField
                  className={classes.textInput}
                  inputRef={register}
                  variant="outlined"
                  name="city"
                  label="City"
                />
              </Box>
            </Box>
            <Box className={classes.formGroupContainer}>
              <Typography className={classes.subTitle}>Arrival date</Typography>
              <Box className={classes.formGroup}>
                <DatePickerComponent
                  ref={register}
                  name="startArrival"
                  label="Start Arrival"
                  isDiscardedChange={isDiscardedChange}
                />
                <DatePickerComponent
                  ref={register}
                  name="endArrival"
                  label="End Arrival"
                  isDiscardedChange={isDiscardedChange}
                />
              </Box>
            </Box>
            <Box className={classes.formGroupContainer}>
              <Typography className={classes.subTitle}>
                Departure date
              </Typography>
              <Box className={classes.formGroup}>
                <DatePickerComponent
                  ref={register}
                  name="startDeparture"
                  label="Start Departure"
                  isDiscardedChange={isDiscardedChange}
                />
                <DatePickerComponent
                  ref={register}
                  name="endDeparture"
                  label="End Departure"
                  isDiscardedChange={isDiscardedChange}
                />
              </Box>
            </Box>
            <Box className={classes.buttonGroup}>
              <PrimaryButton type="submit">
                <div>
                  <FontAwesomeIcon icon={faSearch} size="lg" />
                  <span className={classes.buttonText}>Search</span>
                </div>
              </PrimaryButton>
              <PrimaryButton type="reset" handleClick={clearForm}>
                <div>
                  <FontAwesomeIcon icon={faTrash} size="lg" />
                  <span className={classes.buttonText}>Clear</span>
                </div>
              </PrimaryButton>
              <PrimaryButton type="button" handleClick={resetSearchFilters}>
                <div>
                  <FontAwesomeIcon icon={faSync} size="lg" />
                  <span className={classes.buttonText}>Reset</span>
                </div>
              </PrimaryButton>
            </Box>
          </form>
        </Box>
      )}
    </Box>
  );
};

const mapDispatchToProps = (dispatch) => ({
  actions: {
    fetchTravelings: (travelingOptions) =>
      dispatch(fetchTravelings(travelingOptions)),
  },
});

export default connect(null, mapDispatchToProps)(SearchPanel);
