import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  labelSwitcher: {
    marginLeft: 0,
  },
  mainTitle: {
    textAlign: "center",
    textTransform: "uppercase",
  },
  subTitle: {
    marginBottom: 20,
  },
  formGroupContainer: {
    margin: "15px 0",
  },
  formGroup: {
    display: "flex",
    justifyContent: "space-between",
  },
  textInput: {
    width: "48%",
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "center",
  },
  buttonText: {
    margin: 10,
  },
});
