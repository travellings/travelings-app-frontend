import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useStyles } from './styles';

const SecondarySpinner = () => {
  const classes = useStyles();

  return (
    <Box className={classes.spinner}>
      <Typography className={classes.title}>Loading</Typography>
      <CircularProgress className={classes.circularProgress} thickness={1} />
    </Box>
  );
};

export default SecondarySpinner;
