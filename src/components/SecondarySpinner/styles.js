import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  spinner: {
    width: 80,
    height: 80,
    margin: '60px auto',
    position: 'relative',
  },
  title: {
    margin: 'auto',
    padding: '30px 0',
    fontSize: 12,
    fontWeight: 100,
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#3f51b5',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 1,
  },
  circularProgress: {
    width: '80px !important',
    height: '80px !important',
  },
});
