import React from 'react';
import Container from '@material-ui/core/Container';
import { ToastContainer } from 'react-toastify';
import TravelingsTableContainer from '../TravelingsTableContainer';
import withToastNotifications from '../../HOC/withToastNotifications';

const App = () => {
  return (
    <>
      <ToastContainer autoClose={2500} />
      <Container maxWidth='md'>
        <TravelingsTableContainer />
      </Container>
    </>
  );
};

export default withToastNotifications(App);
