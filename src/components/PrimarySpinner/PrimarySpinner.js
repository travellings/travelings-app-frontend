import React from 'react';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useStyles, useStylesCircularProgress } from './styles';

const PrimarySpinner = () => {
  const classes = useStyles();
  const classesCircularProgress = useStylesCircularProgress();

  return (
    <div className={clsx(classes.screen, classes.screenLoading)}>
      <div className={classes.loader}>
        <h3 className={classes.text}>Loading</h3>
        <CircularProgress classes={classesCircularProgress} thickness={1} />
      </div>
    </div>
  );
};

export default PrimarySpinner;
