import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  loader: {
    width: 140,
    height: 140,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  text: {
    margin: 'auto',
    padding: '52px 28px',
    fontSize: 18,
    textTransform: 'uppercase',
    textAlign: 'center',
    fontWeight: 100,
    color: 'white',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 1,
  },
  screenLoading: {
    backgroundColor: 'rgba(87 156 190 / 0.85)',
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1400,
  },
});

export const useStylesCircularProgress = makeStyles({
  root: {
    width: '140px !important',
    height: '140px !important',
  },
  colorPrimary: {
    color: 'white',
  },
});
