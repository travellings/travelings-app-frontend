import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  fetchTravelings,
  handleModal,
  setCurrentSelectedTraveling,
  deleteTraveling,
  closeModal,
} from "../../redux";
import TravelingsTableComponent from "../TravelingsTableComponent";
import initialTableState from "./initialTableState";

const TravelingsTableContainer = ({ actions }) => {
  const [anchorElementMenu, setAnchorElementMenu] = useState(null);
  const [tableSettings, setTableSettings] = useState(initialTableState);

  const hideModal = () => actions.closeModal();

  const closeConfigureTravelingMenu = () => setAnchorElementMenu(null);

  const sortHandler = (property) => () => handleRequestSort(property);

  const openConfigureTravelingMenu = (traveling) => ({ currentTarget }) => {
    actions.setCurrentSelectedTraveling(traveling);
    setAnchorElementMenu(currentTarget);
  };

  const openEditTravelingModal = (bool) => () => {
    actions.handleModalVisibility(bool);
    setAnchorElementMenu(null);
  };

  const defineSortOrder = (property) => {
    const { sortBy, sortOrder } = tableSettings;
    const isAsc = sortBy === property && sortOrder === "asc";
    return isAsc ? "desc" : "asc";
  };

  const handleTableSettings = ({
    country,
    city,
    startArrival,
    endArrival,
    startDeparture,
    endDeparture,
  }) => {
    setTableSettings({
      ...tableSettings,
      filterWordByCountry: country,
      filterWordByCity: city,
      startArrivalDate: startArrival,
      endArrivalDate: endArrival,
      startDepartureDate: startDeparture,
      endDepartureDate: endDeparture,
    });
  };

  const handleChangePage = (event, newPage) => {
    actions.fetchTravelings({
      fetchData: {
        ...tableSettings,
        currentPageNumber: newPage,
      },
      coverFullScreen: false,
    });

    setTableSettings({
      ...tableSettings,
      currentPageNumber: newPage,
    });
  };

  const handleChangeRowsPerPage = (event) => {
    actions.fetchTravelings({
      fetchData: {
        ...tableSettings,
        currentPageNumber: 0,
        rowsPerPage: parseInt(event.target.value, 10),
      },
      coverFullScreen: false,
    });

    setTableSettings({
      ...tableSettings,
      currentPageNumber: 0,
      rowsPerPage: parseInt(event.target.value, 10),
    });
  };

  const handleRequestSort = (property) => {
    actions.fetchTravelings({
      fetchData: {
        ...tableSettings,
        sortBy: property,
        sortOrder: defineSortOrder(property),
      },
      coverFullScreen: false,
    });

    setTableSettings({
      ...tableSettings,
      sortBy: property,
      sortOrder: defineSortOrder(property),
    });
  };

  useEffect(() => {
    actions.fetchTravelings({
      fetchData: tableSettings,
      coverFullScreen: true,
    });
  }, []); // eslint-disable-line

  return (
    <TravelingsTableComponent
      tableSettings={tableSettings}
      anchorElementMenu={anchorElementMenu}
      hideModal={hideModal}
      handleChangePage={handleChangePage}
      sortHandler={sortHandler}
      openEditTravelingModal={openEditTravelingModal}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      openConfigureTravelingMenu={openConfigureTravelingMenu}
      closeConfigureTravelingMenu={closeConfigureTravelingMenu}
      handleTableSettings={handleTableSettings}
      deleteTraveling={actions.deleteTraveling}
    />
  );
};

const mapDispatchToProps = (dispatch) => ({
  actions: {
    fetchTravelings: (travelingOptions) =>
      dispatch(fetchTravelings(travelingOptions)),
    handleModalVisibility: (modalVisibility) =>
      dispatch(handleModal(modalVisibility)),
    setCurrentSelectedTraveling: (currentSelectedTraveling) =>
      dispatch(setCurrentSelectedTraveling(currentSelectedTraveling)),
    deleteTraveling: (travelingId) => dispatch(deleteTraveling(travelingId)),
    closeModal: () => dispatch(closeModal),
  },
});

export default connect(null, mapDispatchToProps)(TravelingsTableContainer);
