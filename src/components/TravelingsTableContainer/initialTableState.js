const initialTableState = {
  sortBy: "arrival",
  sortOrder: "asc",
  currentPageNumber: 0,
  rowsPerPage: 10,
  country: "",
  city: "",
  arrivalRange: {
    startDate: "",
    endDate: "",
  },
  departureRange: {
    startDate: "",
    endDate: "",
  },
};

export default initialTableState;
