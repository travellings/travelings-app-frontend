import React, { useState } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import ConfirmPopover from '../ConfirmPopover';

const ConfigureTravelingMenu = ({
  anchorElementMenu,
  closeConfigureTravelingMenu,
  openEditTravelingModal,
  deleteTraveling,
  hideModal,
}) => {
  const [anchorConfirmPopover, setAnchorConfirmPopover] = useState(null);

  const detachAnchorConfirmPopover = () => setAnchorConfirmPopover(null);
  const attachAnchorConfirmPopover = ({ target }) => {
    setAnchorConfirmPopover(target);
  };

  return (
    <>
      <Menu
        anchorEl={anchorElementMenu}
        open={Boolean(anchorElementMenu)}
        onClose={closeConfigureTravelingMenu}
      >
        <MenuItem onClick={openEditTravelingModal(true)}>
          <FontAwesomeIcon icon={faEdit} size='lg' />
          <span>Edit</span>
        </MenuItem>
        <MenuItem onClick={attachAnchorConfirmPopover}>
          <FontAwesomeIcon icon={faTrash} size='lg' />
          <span>Delete</span>
        </MenuItem>
      </Menu>
      <ConfirmPopover
        anchorEl={anchorConfirmPopover}
        closePopover={detachAnchorConfirmPopover}
        hideModal={hideModal}
        deleteTraveling={deleteTraveling}
        textAction='delete'
      />
    </>
  );
};

export default ConfigureTravelingMenu;
