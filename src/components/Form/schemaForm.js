import * as yup from 'yup';

export const schema = yup.object().shape({
  country: yup
    .string()
    .matches(/^(\D*)$/, 'Country should not contain numbers')
    .required('Country is a required field'),
  city: yup
    .string()
    .matches(/^(\D*)$/, 'City should not contain numbers')
    .required('City is a requreid filed'),
  arrival: yup.date().required(),
  departure: yup
    .date()
    .min(
      yup.ref('arrival'),
      'Departure date must be later than or equal to arrival date'
    )
    .required(),
});
