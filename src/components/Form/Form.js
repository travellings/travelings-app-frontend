import React, { useState, useRef } from 'react';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import DatePicker from '../DatePicker';
import PrimaryButton from '../PrimaryButton';
import AutocompleteInput from '../AutocompleteInput';
import ConfirmPopover from '../ConfirmPopover';
import { useStyles } from './styles';
import { countries, getCitiesByCountryCode } from 'country-city-location';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/free-solid-svg-icons';
import { convertDateFormat } from '../../utils';
import { YYYY_MM_DD } from '../../constants';
import { schema } from './schemaForm';

const Form = ({
  editTraveling,
  createTraveling,
  handleModalVisibility,
  currentSelectedTraveling,
  hideModal,
}) => {
  const [submitButtonReference, setSubmitButtonReference] = useState(null);
  const [travelingData, setTravelingData] = useState({});
  const [citiesList, setCitiesList] = useState([]);

  const { register, handleSubmit, errors } = useForm({
    mode: 'onSubmit',
    reValidateMode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const submitButtonRef = useRef();
  const classes = useStyles();

  const countriesAutoCompleteOptions = countries.map((item) => item.Name);

  const countryInputValue = currentSelectedTraveling.country || '';
  const cityInputValue = currentSelectedTraveling.city || '';

  const arrivalDatePickerValue =
    currentSelectedTraveling.arrival ||
    convertDateFormat({ date: new Date(), format: YYYY_MM_DD });
  const departureDatePickerValue =
    currentSelectedTraveling.departure ||
    convertDateFormat({ date: new Date(), format: YYYY_MM_DD });

  const attachButtonReference = () => submitButtonRef.current;
  const detachButtonReference = () => setSubmitButtonReference(null);

  const formateTravelingData = ({ arrival, departure, country, city }) => {
    const formattedArrivalDate = convertDateFormat({
      date: arrival,
      format: YYYY_MM_DD,
    });

    const formattedDepartureDate = convertDateFormat({
      date: departure,
      format: YYYY_MM_DD,
    });

    const cityCaptitalLetter = city[0].toUpperCase() + city.substring(1);
    const countryCaptitalLetter =
      country[0].toUpperCase() + country.substring(1);

    return {
      country: countryCaptitalLetter,
      city: cityCaptitalLetter,
      arrival: formattedArrivalDate,
      departure: formattedDepartureDate,
    };
  };

  const onSubmit = ({ arrival, departure, country, city }) => {
    const travelingData = formateTravelingData({
      country,
      city,
      arrival,
      departure,
    });

    const submitButtonAnchor = attachButtonReference();

    if (Object.keys(currentSelectedTraveling).length > 0) {
      setSubmitButtonReference(submitButtonAnchor);
      setTravelingData(travelingData);
    } else {
      createTraveling(travelingData);
      handleModalVisibility(false);
    }
  };

  const selectCountry = (countryName) => {
    const findingCountry = countries.find(
      (country) => country.Name === countryName
    );

    if (findingCountry) {
      const citiesAutoCompleteOptions = getCitiesByCountryCode(
        findingCountry.Alpha2Code
      ).map((item) => item.name);
      setCitiesList(citiesAutoCompleteOptions);
    }
  };

  return (
    <>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <AutocompleteInput
          ref={register}
          name='country'
          options={countriesAutoCompleteOptions}
          label='Country'
          selectCountry={selectCountry}
          error={!!errors.country}
          helperText={errors?.country?.message}
          value={countryInputValue}
        />
        <AutocompleteInput
          ref={register}
          name='city'
          options={citiesList}
          label='City'
          error={!!errors.city}
          helperText={errors?.city?.message}
          value={cityInputValue}
        />
        <DatePicker
          ref={register}
          label='Arrival'
          name='arrival'
          error={!!errors.arrival}
          helperText={errors?.arrival?.message}
          value={arrivalDatePickerValue}
        />
        <DatePicker
          ref={register}
          label='Departure'
          name='departure'
          error={!!errors.departure}
          helperText={errors?.departure?.message}
          value={departureDatePickerValue}
        />
        <PrimaryButton ref={submitButtonRef} type='submit'>
          <div>
            <FontAwesomeIcon icon={faSave} size='lg' />
            <span className={classes.buttonText}>Save</span>
          </div>
        </PrimaryButton>
        <ConfirmPopover
          travelingData={travelingData}
          editTraveling={editTraveling}
          anchorEl={submitButtonReference}
          closePopover={detachButtonReference}
          hideModal={hideModal}
          textAction='edit'
        />
      </form>
    </>
  );
};

const mapStateToProps = (state) => ({
  currentSelectedTraveling: state.travelings.currentSelectedTraveling,
});

export default connect(mapStateToProps)(Form);
