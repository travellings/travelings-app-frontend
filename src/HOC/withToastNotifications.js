import { connect } from 'react-redux';
import { handleError } from '../redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function withToastNotifications(WrappedComponent) {
  function HOC({ error, actions }) {
    const resetError = () => {
      actions.handleError({ isError: false, errorMessage: '' });
    };

    if (error.isError) {
      toast.error(error.errorMessage, { onClose: resetError });
    }

    return <WrappedComponent />;
  }

  const mapStateToProps = (state) => ({
    error: state.error,
  });

  const mapDispatchToProps = (dispatch) => ({
    actions: {
      handleError: (error) => dispatch(handleError(error)),
    },
  });

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
}

export default withToastNotifications;
