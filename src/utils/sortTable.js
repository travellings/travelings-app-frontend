function descendingComparator({ nextElement, prevElement, currentOrderBy }) {
  if (prevElement[currentOrderBy] < nextElement[currentOrderBy]) {
    return -1;
  }
  if (prevElement[currentOrderBy] > nextElement[currentOrderBy]) {
    return 1;
  }
  return 0;
}

function getComparator({ currentOrder, currentOrderBy }) {
  return currentOrder === 'desc'
    ? (nextElement, prevElement) =>
        descendingComparator({ nextElement, prevElement, currentOrderBy })
    : (nextElement, prevElement) =>
        -descendingComparator({ nextElement, prevElement, currentOrderBy });
}

function stabilizeArray(array) {
  return array.map((el, index) => [el, index]);
}

export function stableSort({ array, currentOrder, currentOrderBy }) {
  const stabilizedArray = stabilizeArray(array);

  const gettedComparator = getComparator({
    currentOrder,
    currentOrderBy,
  });

  stabilizedArray.sort((nextElement, prevElement) => {
    const order = gettedComparator(nextElement[0], prevElement[0]);
    if (order !== 0) return order;
    return nextElement[1] - prevElement[1];
  });
  return stabilizedArray.map((el) => el[0]);
}

export function sliceArrayPerPage({ array, page, rowsPerPage }) {
  return array.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
}

export function sortAndSliceArrayPerPage({
  array,
  currentOrder,
  currentOrderBy,
  page,
  rowsPerPage,
}) {
  return sliceArrayPerPage({
    array: stableSort({ array, currentOrder, currentOrderBy }),
    page,
    rowsPerPage,
  });
}
