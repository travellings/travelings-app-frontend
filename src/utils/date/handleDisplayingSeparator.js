import * as POSITIONS from '../../constants';

// ^\d{2}\-\d{2}\-\d{4}$
// ^[0-3](?:(?<=0|1|2)[0-9]|(?<=3)[0-1])-[0-1](?:(?<=0)[1-9]|(?<=1)[0-2])-\d{4}$

export function handleDisplayingSeparator(value) {
  return {
    displayDateSeparator:
      value.length === POSITIONS.POSITION_AFTER_DATE &&
      value[POSITIONS.POSITION_BEFORE_DATE] !== '-',
    displayMonthSeparator:
      value.length === POSITIONS.POSITION_AFTER_MONTH &&
      value[POSITIONS.POSITION_BEFORE_MONTH] !== '-',
    deleteDateSeparator:
      value.length === POSITIONS.POSITION_AFTER_DATE &&
      value[POSITIONS.POSITION_BEFORE_DATE] === '-',
    deleteMonthSeparator:
      value.length === POSITIONS.POSITION_AFTER_MONTH &&
      value[POSITIONS.POSITION_BEFORE_MONTH] === '-',
  };
}
