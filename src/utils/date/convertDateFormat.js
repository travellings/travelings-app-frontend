import moment from 'moment';

export function convertDateFormat({ date, format }) {
  return moment(date).format(format);
}
