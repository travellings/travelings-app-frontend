export function reverseDate(date) {
  return date.split('-').reverse().join('-');
}
