import { handleDisplayingSeparator } from './handleDisplayingSeparator';
import { insertSeparatorIntoDate } from './insertSeparatorIntoDate';
import { deleteSeparatorFromDate } from './deleteSeparatorFromDate';
import * as POSITIONS from '../../constants';

export function joinValueInput(value) {
  let splittedValue = value.split('');

  const {
    displayDateSeparator,
    displayMonthSeparator,
    deleteDateSeparator,
    deleteMonthSeparator,
  } = handleDisplayingSeparator(splittedValue);

  if (displayDateSeparator) {
    insertSeparatorIntoDate({
      value: splittedValue,
      position: POSITIONS.POSITION_BEFORE_DATE,
    });
  }

  if (displayMonthSeparator) {
    insertSeparatorIntoDate({
      value: splittedValue,
      position: POSITIONS.POSITION_BEFORE_MONTH,
    });
  }

  if (deleteDateSeparator) {
    deleteSeparatorFromDate({
      value: splittedValue,
      position: POSITIONS.POSITION_BEFORE_DATE,
    });
  }

  if (deleteMonthSeparator) {
    deleteSeparatorFromDate({
      value: splittedValue,
      position: POSITIONS.POSITION_BEFORE_MONTH,
    });
  }

  return splittedValue.join('');
}
