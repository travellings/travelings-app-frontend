import moment from 'moment';
import { sortAndSliceArrayPerPage } from './sortTable';

export function filterArrayByDateRange({ key, array, startDate, endDate }) {
  const rangeArray = array.filter((item) => {
    return moment(item[key]).isBetween(
      moment(startDate).subtract(1, 'day'),
      moment(endDate).add(1, 'day')
    );
  });

  return rangeArray.length > 0 ? rangeArray : array;
}

export function chooseShortestArrayBetweenTwo({ firstArray, secondArray }) {
  if (firstArray.length > 0 && secondArray.length > 0) {
    return firstArray.length < secondArray.length ? firstArray : secondArray;
  }
  return firstArray.length > secondArray.length ? firstArray : secondArray;
}

export function chooseShortestArrayBetweenThree({
  firstArray,
  secondArray,
  thirdArray,
}) {
  const arrayLength = [
    firstArray.length,
    secondArray.length,
    thirdArray.length,
  ];

  const arrays = [firstArray, secondArray, thirdArray];
  let minimalArrayLength = arrays[0];

  for (let i = 0; i < arrayLength.length; i++) {
    if (arrayLength[i] < minimalArrayLength.length && arrayLength[i] > 0) {
      minimalArrayLength = arrays[i];
    }
  }

  return minimalArrayLength;
}

export function filterDataTableByWord({ array, key, word }) {
  return array.filter(
    (item) => item[key].toLowerCase().indexOf(word.toLowerCase()) !== -1
  );
}

export function filterDataTableByCountry({
  dataTableOrder,
  initialArray,
  currentOrder,
  currentOrderBy,
  page,
  rowsPerPage,
  arrayArrivalDates,
  arrayDepartureDates,
  filterWordByCountry,
  key,
}) {
  const initialDataTable =
    dataTableOrder.length > 0 && dataTableOrder.length < initialArray.length
      ? dataTableOrder
      : sortAndSliceArrayPerPage({
          array: initialArray,
          currentOrder,
          currentOrderBy,
          page,
          rowsPerPage,
        });

  const shortestArrayByDates = chooseShortestArrayBetweenTwo({
    firstArray: arrayArrivalDates,
    secondArray: arrayDepartureDates,
  });

  const filterableArray =
    shortestArrayByDates.length > 0 ? shortestArrayByDates : initialDataTable;

  const filteredDataTable = filterDataTableByWord({
    array: filterableArray,
    key,
    word: filterWordByCountry,
  });

  return filteredDataTable;
}

export function filterDataTableByCity({
  dataTableOrder,
  initialArray,
  currentOrder,
  currentOrderBy,
  page,
  rowsPerPage,
  arrayCountry,
  arrayArrivalDates,
  arrayDepartureDates,
  filterWordByCity,
  key,
}) {
  const initialDataTable =
    dataTableOrder.length > 0 && dataTableOrder.length < initialArray.length
      ? dataTableOrder
      : sortAndSliceArrayPerPage({
          array: initialArray,
          currentOrder,
          currentOrderBy,
          page,
          rowsPerPage,
        });

  const shortestArray = chooseShortestArrayBetweenThree({
    firstArray: arrayCountry,
    secondArray: arrayArrivalDates,
    thirdArray: arrayDepartureDates,
  });

  const filterableArray =
    shortestArray.length > 0 ? shortestArray : initialDataTable;

  const filteredDataTable = filterDataTableByWord({
    array: filterableArray,
    key,
    word: filterWordByCity,
  });

  return filteredDataTable;
}
