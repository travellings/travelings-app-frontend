import { v4 as uuidv4 } from 'uuid';

export const tableHeadCellNameList = [
  { id: uuidv4(), cellKey: 'number', cellContent: '#' },
  { id: uuidv4(), cellKey: 'country', cellContent: 'Country' },
  { id: uuidv4(), cellKey: 'city', cellContent: 'City' },
  { id: uuidv4(), cellKey: 'arrival', cellContent: 'Arrival' },
  { id: uuidv4(), cellKey: 'departure', cellContent: 'Departure' },
];
